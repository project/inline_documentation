INTRODUCTION
------------

Add and view documentation in realtime on pages. Alternative names for this
module could be "Contextual Documentation" or "Realtime Documentation" but
inline has a nice ring to it and matches the way documentation is placed in
code.

When enabled, this module will create a content type "Inline Documentation"
and add a round blue button to the bottom right corner of every page. This
button toggles a panel containing a list of the inline documentation nodes.

Documentation can be added from this panel. With the right permissions, a
button with a plus will appear next to the overview toggle button. This button
spawns a node add form. The form contains a "Select element" button which
allows the user to select a DOM element on the page on which this piece of
documentation applies to. The element will be highlighted when the
documentation is viewed.

The form also contains a field in which you can specify on which pages the
documentation should be shown. This works exactly the same as the page
functionallity in the blocks module.

REQUIREMENTS
------------

No requirements.

RECOMMENDED MODULES
-------------------

 * Node View Permissions

   URL: https://www.drupal.org/project/node_view_permissions

   Inline Documentation is stored in nodes which by default are accessible to
   anyone with the "Access content" permission. The "Node View Permissions"
   module allows you to restrict access per content type.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

To view inline documentation, the following permissions are required:

 * View inline documentation overview

   Adds a round blue button to the bottom right corner of the page. Clicking
   this will open a panel with a list of inline documentation nodes.

 * Access content

   Drupal doesn't come out of the box with view permissions per content type so
   it's all or nothing. This means that inline documentation nodes are
   accessible like any other nodes. See the recommended modules section for a
   possible solution to this.

To add/edit inline documentation, the following permissions are required:

 * Access contextual links

   The edit link is altered to redirect to a custom node edit page.

 * Create inline_documentation content

   Adds a node add form to the overview.

 * Edit any inline_documentation content or Edit own inline_documentation
   content

   Use this in combination with the "Access contextual links" permission.

 * Administer nodes

   Without this permission, the user is unable to publish or unpublish inline
   documentation. All content is published by default.

 * View own unpublished content

   Only required when the user is able to unpublish inline documentation.
   Without this permission, the node will disappear from the overview once
   unpublished. This means the user can only edit the node again from the
   content overview.

It's up to you if you want to give someone the permission to delete inline
documentation or do stuff with the revisions. These permissions do not impact
the inline documentation functionallity.

To control on which pages the inline documentation overview is shown, a
whitelist and blacklist setting is available on the settings page which
can be found here: Administration » Settings » Content » Inline Documentation
Settings.

The permission "Manage inline documentation settings" is required to access
this page.
