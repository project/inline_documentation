<?php

namespace Drupal\inline_documentation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the settings form for this module.
 */
class InlineDocSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'inline_documentation_settings_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['visibility'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Inline Documentation overview visibility'),
    ];

    $form['visibility']['explanation'] = [
      '#markup' => $this->t("<p>Here, you can specify on which pages the inline documentation overview should or should not be shown. You can do this by adding page paths to the whitelist or blacklist field.<br />Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is <em>/node/*</em> for every node page. <em>&#60;front&#62;</em> is the front page.</p>"),
    ];

    // Whitelist pages.
    $form['visibility']['inline_documentation_page_whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Page whitelist'),
      '#description' => $this->t("The pages on which you want to show the inline documentation overview. Leave empty to show the documentation on all pages except the blacklisted ones."),
      '#default_value' => $this->config('inline_documentation.settings')->get('inline_documentation_page_whitelist'),
      '#rows' => 5,
    ];

    // Blacklist pages.
    $form['visibility']['inline_documentation_page_blacklist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Page blacklist'),
      '#description' => $this->t("The pages on which you <strong>don't</strong> want to show the inline documentation overview."),
      '#default_value' => $this->config('inline_documentation.settings')->get('inline_documentation_page_blacklist'),
      '#rows' => 5,
    ];

    // Group submit handlers in an actions element with a key of "actions".
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the settings.
    $this->configFactory()
      ->getEditable('inline_documentation.settings')
      ->set('inline_documentation_page_whitelist', $form_state->getValue('inline_documentation_page_whitelist'))
      ->set('inline_documentation_page_blacklist', $form_state->getValue('inline_documentation_page_blacklist'))
      ->save();

    // Show message.
    $this->messenger()->addMessage($this->t('Settings saved.'));
  }

}
