/**
 * @file
 * Initiate inline documentation.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Process inline_documentation elements.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches inline_documentation behaviors.
   */
  Drupal.behaviors.inline_documentation = {
    attach: function (context, settings) {

      // Only execute the rest of this script when the context is the document.
      if (context !== document) {
        return;
      }

      // Define elements.
      const inline_documentation = $('.inline-documentation', context);
      const toggle_button = $('#toggle-inline-documentation', context);
      const overview = $('.inline-documentation-overview', context);
      const doc_title = overview.find('.inline-doc .doc-title', context);
      const form = $('.inline-documentation-form', context);
      const add_button = $('#add-inline-documentation', context);
      const color_field = $('[data-drupal-selector="edit-field-inline-doc-color-0-value"]', context);
      const page_element_field = $('[data-drupal-selector="edit-field-inline-doc-page-element-1-value"]', context);
      const select_element_button = $('.inline-documentation-select-element', context);

      // Other vars.
      var page_element_field_offset = null;
      var mouse_attachment = null;
      var enable_selector = false;
      var current_element = null;
      var selector_color = '';
      const ignore_elements = [document.body, document.documentElement, document];
      const query_params = new URLSearchParams(window.location.search);

      // If the query parameter 'edit-inline-doc' exists, open the
      // overview + form.
      if (query_params.has('edit-inline-doc')) {
        toggleInlineDocumentation();
        toggleForm('open');
      }

      /**
       * Handle event when the toggle button is clicked.
       */
      toggle_button.click(function (e) {
        // Open or close the overview.
        toggleInlineDocumentation();

        e.preventDefault();
        return false;
      });


      /**
       * Handle event when a documentation title is clicked.
       */
      doc_title.click(function (e) {
        // Get the container of the clicked link.
        let inline_doc = $(this).closest('.inline-doc');

        // Open/close the doc content.
        inline_doc.toggleClass('open');

        // Stop if the container has no data-field-inline-doc-page-element
        // attribute.
        if (typeof inline_doc.attr('data-field-inline-doc-page-element') === 'undefined') {
          e.preventDefault();
          return false;
        }

        // Generate a unique id (hashcode).
        let uid = inline_doc.attr('data-field-inline-doc-page-element').hashCode();
        // Turn the attribute into an actual element.
        let element = $(inline_doc.attr('data-field-inline-doc-page-element'));

        // Stop if the element does not exist.
        if (element.length == 0) {
          e.preventDefault();
          return false;
        }

        // Check if the container is open.
        if (inline_doc.hasClass('open')) {
          // Set a default color for the highlight element.
          var color = '#006D88';
          // If the container has the attribute data-field-inline-doc-color, use that
          // as color.
          if (typeof inline_doc.attr('data-field-inline-doc-color') !== 'undefined') {
            color = inline_doc.attr('data-field-inline-doc-color');
          }

          // Highlight the element.
          highlightElement(element, uid, color);

          // We want to scroll the element into sight when it's off screen so
          // set some vars for this, starting with the top and bottom
          // position of the screen.
          let screen_top = $(window).scrollTop();
          let screen_bottom = $(window).height() + $(window).scrollTop();

          // Check if there's an admin toolbar and if so, get its height.
          let toolbarTabOuterHeight = $('#toolbar-bar').find('.toolbar-tab').outerHeight() || 0;
          let toolbarTrayHorizontalOuterHeight = $('.is-active.toolbar-tray-horizontal').outerHeight() || 0;
          let toolbarHeight = toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight;
          // Add the height to the top of the screen as the toolbar actually
          // hides content.
          screen_top += toolbarHeight;

          // Check if the element is beneath the bottom of the screen.
          if (element.offset().top > screen_bottom) {
            // Scroll the element slowly into view.
            $([document.documentElement, document.body]).animate({
              // Calculate the position by these rules:
              // - Subtract window height from element top: places the
              //   scrollbar just above the element, the element is still not
              //   visible.
              // - Add element height: brings the element into view, but
              //   placed flat against the bottom of the screen.
              // - Add 20px: some margin below the element.
              scrollTop: element.offset().top - $(window).height() + element.height() + 20
            }, 'slow');
          }
          // Check if the element is above the top of the screen.
          else if (element.offset().top < screen_top) {
            // Scroll the element slowly into view.
            $([document.documentElement, document.body]).animate({
              // Calculate the position by these rules:
              // - Subtract toolbar height from element top: brings the
              //   element into view, either right below the admin toolbar or
              //   against the top of the screen.
              // - Subtract 20px: some margin above the element.
              scrollTop: element.offset().top - toolbarHeight - 20
            }, 'slow');
          }
        }
        else {
          // Remove the highlight from the element.
          deHighlightElement(uid);
        }

        e.preventDefault();
        return false;
      });

      /**
       * Handle event when the add button is clicked.
       */
      add_button.click(function (e) {
        // Open or close the form.
        toggleForm();

        e.preventDefault();
        return false;
      });

      /**
       * Handle event when the select element button is clicked.
       */
      select_element_button.click(function (e) {
        // Create an element showing help text attached to the mouse cursor.
        $('body').append('<div id="inline-documentation-selector-help">' + Drupal.t('Select an element') + '</div>');
        // Put it in a var.
        mouse_attachment = $('#inline-documentation-selector-help');

        // Load the color from the color field.
        selector_color = color_field.val();
        // Get the position of the page element field.
        page_element_field_offset = page_element_field.offset();

        // Enable the selector.
        enable_selector = true;

        // Hide the inline documentation during the element selecting.
        inline_documentation.fadeOut();
        return false;
      });

      /**
       * Handle event when the escape key is pressed.
       */
      $(document).on('keydown', function (event) {
        if (event.key == "Escape") {
          // Close the selector if it's enable.
          if (enable_selector) {
            closeSelector();
          }
          // Otherwise, hide the form if it's visible.
          else if (form.hasClass('show')) {
            toggleForm('close');
          }
          // Otherwise, show/hide the overview.
          else {
            toggleInlineDocumentation();
          }
        }
      });

      /**
       * Highlight the elements where the mouse hovers above when the selector
       * is enabled.
       */
      $(document).mousemove(function(e) {
        // Do nothing if the selector is not enabled.
        if (enable_selector === false) {
          return;
        }

        // Update the mouse attachment to match the position of the mouse
        // cursor.
        mouse_attachment.css({
          top: e.pageY,
          left: e.pageX
        });

        // Don't do anything if the target is the current element.
        if (e.target === current_element) {
          return;
        }

        // Remove the highlight element if the target is in the ignore_elements
        // array.
        if (~ignore_elements.indexOf(e.target)) {
          current_element = null;
          deHighlightElement('selector');
          return;
        }

        // Put a highlight element on top of the target.
        highlightElement($(e.target), 'selector', selector_color);

        // Update the current element with the target.
        current_element = e.target;
      });

      /**
       * Catch the click event on link and non-link elements when the selector
       * is enabled.
       */
      $(document, 'a').click(function (e) {
        // Do nothing if the selector is not enabled.
        if (enable_selector === false) {
          return;
        }

        // Select the clicked element.
        selectElement(e.target);

        e.stopImmediatePropagation();
        return false;
      });

      /**
       * Helper functions from here.
       */

      /**
       * Get the dom path of the element and put it in a form field.
       *
       * @param {object} el
       *   DOM element object.
       */
      function selectElement(el) {
        // Get the dom path of the element.
        let id = getDomPath(el);

        // Put the dom path in the page element field.
        page_element_field.val(id);

        // Close the selector.
        closeSelector(false);

        // Move the mouse attachment animated to the page element field to let
        // the user know what happened.
        mouse_attachment.animate({
          top: page_element_field_offset.top + 'px',
          left: page_element_field_offset.left + 'px'
        }, 300, function () {
          // Remove the attachment once the position is reached.
          mouse_attachment.remove();
          // Trigger a flash animation on the field by adding a class.
          page_element_field.addClass('flash');

          // The flash animation has a duration of 1 sec so remove the class
          // after that time so it can be re-used.
          setTimeout(function () {
            page_element_field.removeClass('flash');
          }, 1000);
        });
      }

      /**
       * Redirect to a page with node edit form or back to original page.
       *
       * @param {int} nid
       *   The nid of the node to edit.
       */
      function toggleNodeEditPage(nid = null) {
        // Get the current path and prefix it with a slash.
        let new_path = '/' + drupalSettings.path.currentPath;

        // Create the currentQuery property if it doesn't exist.
        if (!drupalSettings.path.hasOwnProperty('currentQuery')) {
          drupalSettings.path.currentQuery = {};
        }

        // Get the current query parameters.
        let current_query = drupalSettings.path.currentQuery;

        // If the q param exist, delete is because it contains the path.
        if (current_query.hasOwnProperty('q')) {
          delete current_query.q;
        }

        // If a nid is given, add it to the query.
        if (nid) {
          current_query['edit-inline-doc'] = nid;
        }
        // If not, check if it's in the query and remove it.
        else if (current_query.hasOwnProperty('edit-inline-doc')) {
          delete current_query['edit-inline-doc'];

          // Also remove the destination if it is set. This is automatically
          // added by the contextual module.
          if (current_query.hasOwnProperty('destination')) {
            delete current_query['destination'];
          }
        }

        // Container for the parameters.
        var params = '';

        // Check if the query object has any items.
        if (Object.keys(current_query).length > 0) {
          // Iterate through the items in the query object.
          for (let i in current_query) {
            // The params string should start with a question mark.
            if (params == '') {
              params += '?';
            }
            // So if the string is not empty, append a ampersand.
            else {
              params += '&';
            }

            // Add the parameter to the string.
            params += i + '=' + current_query[i];
          }
        }

        // Append the parameters to the path.
        new_path += params;

        // Redirect to user the new path.
        window.location.href = new_path;
      }

      /**
       * Toggle the overview.
       */
      function toggleInlineDocumentation() {
        // Reset all inline doc elements when closing the overview.
        if (overview.hasClass('show')) {
          toggleForm('close');
          $('.inline-doc').removeClass('open');
          $('.inline-documentation-selector-overlay').fadeOut();
        }

        // Show/hide the documentation overview and add button.
        overview.toggleClass('show');
        add_button.toggleClass('show');
      }

      /**
       * Open or close the node add/edit form.
       *
       * @param {string} status
       *   Can either be:
       *   - open: force the form to open or stay open.
       *   - close: force the form to close or stay close.
       *   - null or empty: toggle the form.
       */
      function toggleForm(status = null) {
        // Check if the form is open.
        if (form.hasClass('show')) {
          // Check if the user is editing an existing node.
          if (form.find('.edit-form').length > 0) {
            // If so, show a dialog if the user really wants to stop editing.
            if (confirm('Are you sure you want to stop editing this content?')) {
              // When positive, redirect the user back to the original page.
              toggleNodeEditPage();
            }
            else {
              return;
            }
          }
          // If the form contains an error message, redirect the user back to
          // the original page.
          else if (form.find('.error').length > 0) {
            toggleNodeEditPage();
          }
        }

        // If the status is 'open', open the form if it's closed and do nothing
        // otherwise.
        if (status == 'open') {
          if (!form.hasClass('show')) {
            form.addClass('show');
            add_button.addClass('close');
          }
        }
        // If the status is 'close', close the form if it's open and do nothing
        // otherwise.
        else if (status == 'close') {
          if (form.hasClass('show')) {
            form.removeClass('show');
            add_button.removeClass('close');
          }
        }
        // If no status is given, perform a toggle.
        else {
          form.toggleClass('show');
          add_button.toggleClass('close');
        }
      }

      /**
       * Highlight a dom element.
       *
       * @param {object} element
       *   A dom object.
       * @param {string} uid
       *   The unique identifier of the highlight element.
       * @param {string} color
       *   Contains a color in hex format.
       */
      function highlightElement(element, uid = null, color = null) {
        // Do nothing if the given element doesn't exist.
        if (element.length == 0) {
          return;
        }

        // Create an highlight/overlay element if it doesn't exist.
        if ($('#inline-documentation-element-' + uid).length == 0) {
          $('body').append('<div id="inline-documentation-element-' + uid + '" class="inline-documentation-selector-overlay"></div>');
        }

        // Put the highlight/overlay element in a var.
        let overlay = $('#inline-documentation-element-' + uid);
        // Get the position of the element to put the highlight on.
        let offset = element.offset();

        // Set a default color if none is given.
        if (!color) {
          color = '#006D88';
        }

        // Style and position the highlight/overlay element.
        overlay.css({
          top: offset.top - 4,
          left: offset.left - 4,
          width: element.outerWidth(),
          height: element.outerHeight(),
          backgroundColor: color,
          opacity: .5
        }).fadeIn('fast');
      }

      /**
       * Removes the highlight from a dom element.
       *
       * @param {string} uid
       *   The unique identifier of the highlight element.
       */
      function deHighlightElement(uid = null) {
        $('#inline-documentation-element-' + uid).fadeOut('fast', function () {
          $(this).remove();
        });
      }

      /**
       * Get the dom path of an element.
       *
       * @param {object} el
       *   The element to get the dom path for.
       *
       * @returns
       *   String containing a CSS selector path to the element.
       */
      function getDomPath(el) {
        // Set the var to put the path into.
        const stack = [];

        // Loop through all the parents of the given element.
        while (el.parentNode !== null) {
          let sibCount = 0;
          let sibIndex = 0;

          // Loop through all the children of the parent.
          for (let i = 0; i < el.parentNode.childNodes.length; i++) {
            // Put the child element, aka sibling, into a var.
            const sib = el.parentNode.childNodes[i];

            // Only look for siblings with the same type as the given element.
            if (sib.nodeName === el.nodeName) {
              // Ignore divs with the class 'contextual' because not everyone
              // sees these divs.
              if (sib.classList.contains('contextual')) {
                continue;
              }

              // If the child/sib is the given element, save its index.
              if (sib === el) {
                sibIndex = sibCount;
              }

              // Increase the siblings count.
              sibCount++;
            }
          }

          // Get a save string in lowercase of the element type.
          const nodeName = CSS.escape(el.nodeName.toLowerCase())

          // Ignore 'html' as a parent node.
          if (nodeName === 'html') {
            break;
          }

          // Check if the given element has an id.
          if (el.hasAttribute('id') && el.id !== '') {
            // Add it to the stack and since an id is unique, don't look
            // further.
            stack.unshift(`#${CSS.escape(el.id)}`);
            break;
          }
          // Check if there are more siblings.
          else if (sibCount > 1) {
            // Add the parent to the stack suffixed with 'nth-of-type' to make
            // sure the right element is targeted. Note that 'nth-of-type' is
            // 1-indexed.
            stack.unshift(`${nodeName}:nth-of-type(${sibIndex + 1})`);
          }
          else {
            // There's only one occurence of the element type, so add it to the
            // stack without any additions.
            stack.unshift(nodeName);
          }

          // Update the element to become its parent to move one level up in
          // the dom tree.
          el = el.parentNode;
        }

        // Join the array with '>' so it becomes a jQuery usable string.
        return stack.join(' > ');
      }

      /**
       * Close the element selector.
       *
       * @param {bool} remove_mouse_attachment
       *   If true, also removes the mouse attached div.
       */
      function closeSelector(remove_mouse_attachment = true) {
        // Disable the selector.
        enable_selector = false;
        // Remove the highlight element.
        deHighlightElement('selector');
        // Bring the overview back.
        inline_documentation.fadeIn();
        // Remove the mouse attachment if desired.
        if (remove_mouse_attachment) {
          mouse_attachment.remove();
        }
      }

      /**
       * Create a hash from a string.
       *
       * @returns
       *   Hash code.
       */
      String.prototype.hashCode = function () {
        var hash = 0, i, chr;
        if (this.length === 0) return hash;
        for (i = 0; i < this.length; i++) {
          chr = this.charCodeAt(i);
          hash = ((hash << 5) - hash) + chr;
          hash |= 0;
        }
        return hash;
      };

    }
  };

}(jQuery, Drupal, drupalSettings));
